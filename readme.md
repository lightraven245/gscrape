# Giant Coupon Scraper

### Installation Instructions

* Pull down repository
* Add your Giant credentials into the config.json
* Install Node.JS and NPM
* Open a commmand window in the repo directory and run `npm install`
* Run the application by typing `node .\scraper.js`