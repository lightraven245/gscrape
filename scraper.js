const puppeteer = require('puppeteer');
const config = require('./config');

(async () => {

  // Initialize
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  
  console.log('Beginning run...');

  // Navigate to landing page
  await page.goto('https://giantfoodstores.com/');

  // Open login dialog
  await page.click('body > aside > div > div > div > div > div > div > div.st_footer-button > button')

  // Login
  await page.type('#username', config.username);
  await page.type('body > aside > div > div > div > div > div > div > div > div > div > div.st_container--page > ng-include > site-transition-login > form > div:nth-child(3) > label > input', config.password);

  // Successful login returns 409 for some reason
  try {
    console.log('Logging in...');
    await page.click('body > aside > div > div > div > div > div > div > div > div > div > div.st_container--page > ng-include > site-transition-login > form > button');
    await page.waitForNavigation();
  }
  catch (error) {
    //console.error(error);
  }

  // Navigate to coupons 
  await page.goto('https://giantfoodstores.com/savings/coupons/browse');

  // Click all the coupons
  let baseBody = await page.evaluate(() => document.body.innerHTML);
  let couponLinks = await page.$$('div.item-tile_container.link-pointer > div.item-tile_content > div.item-tile_button-container > button');

  console.log('Found ' + couponLinks.length + ' potential coupon links...');

  await page.$$eval('div.item-tile_container.link-pointer > div.item-tile_content > div.item-tile_button-container > button', links => links.map(link => link.click()));

  console.log('Finished');

  await browser.close();
})();